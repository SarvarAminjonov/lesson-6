class Person2 {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
}
class Baby extends Person2 {
    constructor(name, age, favoriteToy) {
     super(name, age);
     this.toy = favoriteToy;
    }
    play() {
        return `${this.name} is ${this.age} years old, and playing with ${this.toy}`;
    }
}

const richard = new Baby('Richard', 4, 'Buzz Lightyear');
const molly = new Baby('Molly', 3, ' Sheriff Woody');
console.log(richard.play());
console.log(molly.play());